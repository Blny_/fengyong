/*
 * @Author: zkl 3534055259@qq.com
 * @Date: 2022-10-11 11:19:32
 * @LastEditors: zkl 3534055259@qq.com
 * @LastEditTime: 2022-10-24 19:54:23
 * @FilePath: \admin测试\fengyong-admin\src\plugins\elementui.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from "vue";
import {
  Button,
  Tag,
  Form,
  FormItem,
  Input,
  Message,
  Card,
  Container,
  Aside,
  Main,
  Header,
  Menu,
  MenuItem,
  MenuItemGroup,
  Submenu,
  Tooltip,
  Breadcrumb,
  BreadcrumbItem,
  Table,
  TableColumn,
  Pagination,
  Loading,
  Footer,
  Collapse,
  CollapseItem,
  Avatar,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Result,
  Image,
  Select,
  Switch,
  InputNumber,
  Upload,
  Option,
  Dialog,
  Popconfirm,
  Drawer,
  Descriptions,
  DescriptionsItem,
} from "element-ui";
Vue.use(Button);
Vue.use(Tag);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Input);
Vue.use(Card);
Vue.use(Container);
Vue.use(Aside);
Vue.use(Main);
Vue.use(Header);
Vue.use(Menu);
Vue.use(MenuItem);
Vue.use(MenuItemGroup);
Vue.use(Submenu);
Vue.use(Tooltip);
Vue.use(Breadcrumb);
Vue.use(BreadcrumbItem);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Pagination);
Vue.use(Footer);
Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(Avatar);
Vue.use(Dropdown);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Result);
Vue.use(Image);
Vue.use(Select);
Vue.use(Switch);
Vue.use(InputNumber);
Vue.use(Upload);
Vue.use(Option);
Vue.use(Dialog);
Vue.use(Popconfirm);
Vue.use(Drawer);
Vue.use(Descriptions);
Vue.use(DescriptionsItem);
Vue.use(Loading.directive);

Vue.prototype.$loading = Loading.service;
Vue.prototype.$message = Message;
Vue.prototype.$ELEMENT = { size: "small" };
