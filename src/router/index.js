/*
 * @Author: zkl 3534055259@qq.com
 * @Date: 2022-10-11 11:19:32
 * @LastEditors: zkl 3534055259@qq.com
 * @LastEditTime: 2022-10-24 19:30:43
 * @FilePath: \admin测试\fengyong-admin\src\router\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from "vue";
import VueRouter from "vue-router";
import NotFound from '@/components/NotFound.vue'
import login from '@/views/Login/index.vue'
import NProgress from "nprogress";
import "nprogress/nprogress.css";
Vue.use(VueRouter);

const routes = [
  {
    path: '*',
    name: 'NotFound',
    component: NotFound,
    meta: {
      title: "找不到页面",
    },
  },
  {
    path: "/login",
    name: "login",
    component: login,
    meta: {
      title: "登录",
    },
  },
  {
    path: "/upload",
    name: "upload",
    component: () => import("@/components/Upload.vue"),
  },
  {
    path: "/",
    name: "home",
    component: () => import(/* webpackChunkName: "Home" */ "@/views/Home"),
    meta: {
      title: "首页",
    },
    children: [
      {
        path: "",
        alias: "/index",
        name: "index",
        meta: {
          title: "首页",
          closable: false,
        },
        component: () =>
          import(/* webpackChunkName: "Home" */ "@/views/Home/Home.vue"),
      },
      {
        path: "goods",
        name: "goods",
        meta: {
          title: "商品",
          disabled: true,
          key(route) {
            return route.fullPath;
          },
        },
        component: () =>
          import(/* webpackChunkName: "Goods" */ "@/views/Goods"),
        children: [
          {
            path: "",
            alias: "/goods/list",
            name: "goodsList",
            meta: { title: "商品列表" },
            component: () =>
              import(/* webpackChunkName: "Goods" */ "@/views/Goods/List.vue"),
            children: [

            ]
          },
          {
            path: "add",
            name: "goodsAdd",
            meta: { title: "新增商品" },
            component: () =>
              import(/* webpackChunkName: "Goods" */ "@/views/Goods/HandleGoods.vue"),
          },

          {
            path: "edit/:id",
            name: "goodsEdit",
            meta: {
              title: "编辑商品",
            },
            props: true,
            component: () =>
              import(
                /* webpackChunkName: "Goods" */ "@/views/Goods/HandleGoods.vue"
              ),
          },
          {
            path: "detail/:id",
            name: "goodsdetail",
            meta: {
              title: "商品详情",
            },
            props: true,
            component: () =>
              import(
                /* webpackChunkName: "Goods" */ "@/views/Goods/detail.vue"
              ),
          },
        ],
      },
      {
        path: "category",
        name: "category",
        meta: {
          title: "品类",
          disabled: true,
          key(route) {
            return route.fullPath;
          },
        },
        component: () =>
          import(/* webpackChunkName: "Category" */ "@/views/Category"),
        children: [
          {
            path: "/category/list/:id?",
            alias: "/category/list/:id?",
            name: "categoryList",
            meta: {
              title: "品类列表",
            },
            props: true,
            component: () =>
              import(
                /* webpackChunkName: "Category" */ "@/views/Category/Category.vue"
              ),
          },
        ],
      },
      {
        path: "order",
        name: "order",
        meta: {
          title: "订单",
          disabled: true,
          key(route) {
            return route.fullPath;
          },
        },
        component: () =>
          import(/* webpackChunkName: "Order" */ "@/views/Order"),
        children: [
          {
            path: "/order/list/:id?",
            alias: "/order/list/:id?",
            name: "orderList",
            meta: {
              title: "订单列表",
            },
            props: true,
            component: () =>
              import(/* webpackChunkName: "Order" */ "@/views/Order/Order.vue"),
          },
        ],
      },
      {
        path: "user",
        name: "user",
        meta: {
          title: "用户",
          disabled: true,
          key(route) {
            return route.fullPath;
          },
        },
        component: () => import(/* webpackChunkName: "User" */ "@/views/User"),
        children: [
          {
            path: "/user/list",
            alias: "/user/list",
            name: "userList",
            meta: {
              title: "用户列表",
            },
            props: true,
            component: () =>
              import(/* webpackChunkName: "User" */ "@/views/User/User.vue"),
          },
        ],
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// 导航守卫
router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  }
  NProgress.start()
  if (localStorage.getItem('username')) {
    return next();
  }
  if (to.name != 'login') {
    return next({ name: 'login', query: { redirect: from.fullPath } })
  }
  next();
});
router.afterEach((to, from) => {
  NProgress.done();
});
router.onError(() => {
  NProgress.done();
})
export default router;
