/*
 * @Author: zkl 3534055259@qq.com
 * @Date: 2022-10-11 11:19:32
 * @LastEditors: zkl 3534055259@qq.com
 * @LastEditTime: 2022-10-14 11:42:58
 * @FilePath: \admin测试\fengyong-admin\src\main.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from "vue";

import "normalize.css";
// import axios from "axios";
// import http from "@/http";
import VueCookies from "vue-cookies";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/plugins/elementui";
import Fragment from 'vue-fragment'
import RouterTab from 'vue-router-tab'
import 'vue-router-tab/dist/lib/vue-router-tab.css'
import VueFullscreen from 'vue-fullscreen'
Vue.use(VueFullscreen);
Vue.use(Fragment.Plugin);
Vue.config.productionTip = false;
Vue.use(VueCookies);
Vue.use(RouterTab)

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
