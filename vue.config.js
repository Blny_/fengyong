/*
 * @Author: zkl 3534055259@qq.com
 * @Date: 2022-10-11 11:19:32
 * @LastEditors: zkl 3534055259@qq.com
 * @LastEditTime: 2022-10-23 19:46:58
 * @FilePath: \admin测试\fengyong-admin\vue.config.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "" : "/",
  devServer: {
    // port: 3000,
    // proxy: "http://test.raz-kid.cn",
    proxy: {
      "/api": {
        target: "http://admin.raz-kid.cn",
      },
      "/h5": {
        target: "https://fengyong-api.onrender.com",
        pathRewrite: {
          "^/h5": "",
        },
      },
      "/pc": {
        target: "http://192.168.1.42:4000",
        pathRewrite: {
          "^/pc": "",
        },
      },
    },
  },
};

// const { defineConfig } = require("@vue/cli-service");
// module.exports = defineConfig({
//   transpileDependencies: true,

// });
